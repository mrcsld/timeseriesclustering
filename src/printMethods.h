//
// Created by marco on 18/04/19.
//

#ifndef TIMESERIESCLUSTERING_PRINTMETHODS_H
#define TIMESERIESCLUSTERING_PRINTMETHODS_H

#include <string>
#include <vector>
#include <iostream>

using namespace std;

void printLabels(vector<string> labels);

#endif //TIMESERIESCLUSTERING_PRINTMETHODS_H

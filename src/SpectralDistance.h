//
// Created by marco on 02/05/19.
//

#ifndef TIMESERIESCLUSTERING_SPECTRALDISTANCE_H
#define TIMESERIESCLUSTERING_SPECTRALDISTANCE_H

#include <mlpack/prereqs.hpp>
#include <mlpack/core/metrics/lmetric.hpp>
#include <math.h>
#include <vector>
#include <fftw3.h>


class SpectralDistance {
private:
    int input_size = 100;
    int output_size = (input_size/2); // RIMETTERE +1
    double* input_buffer1;
    double* input_buffer2;

    fftw_complex* output_buffer1;
    fftw_complex* output_buffer2;
    int flags = FFTW_ESTIMATE;
    fftw_plan plan1;
    fftw_plan plan2;


public:
    SpectralDistance();
    arma::mat fourierFeatures(arma::mat data);

    double Evaluate(arma::vec a, arma::vec b);

};


#endif //TIMESERIESCLUSTERING_SPECTRALDISTANCE_H

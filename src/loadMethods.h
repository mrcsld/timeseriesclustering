//
// Created by marco on 18/04/19.
//

#ifndef TIMESERIESCLUSTERING_LOADMETHODS_H
#define TIMESERIESCLUSTERING_LOADMETHODS_H

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

std::vector <std::string> readCSVLabels(std::string filename);

#endif //TIMESERIESCLUSTERING_LOADMETHODS_H

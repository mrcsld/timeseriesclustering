//
// Created by marco on 18/04/19.
//

#include "printMethods.h"

void printLabels(vector<string> labels) {

    for (auto const& i: labels) {
        std::cout << i << " ";
    }
    cout<<"\n";
}
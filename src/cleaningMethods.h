//
// Created by marco on 18/04/19.
//

#ifndef TIMESERIESCLUSTERING_CLEANINGMETHODS_H
#define TIMESERIESCLUSTERING_CLEANINGMETHODS_H

#include <mlpack/methods/kmeans/kmeans.hpp>
#include <math.h>
#include "dataTransform.h"

using namespace std;

arma::mat oneRowRemoveAvg(arma::mat data);
arma::mat oneRowRemoveStd(arma::mat data);
arma::mat oneRowStandardize(arma::mat data);
arma::mat oneRowNormalize(arma::mat data);
arma::mat oneRowNormalize2(arma::mat data);
arma::mat oneRowSubstituteNaN(arma::mat data);
arma::mat removeAvg(arma::mat data);
arma::mat removeStd(arma::mat data);
arma::mat substituteNaN(arma::mat data);

#endif //TIMESERIESCLUSTERING_CLEANINGMETHODS_H

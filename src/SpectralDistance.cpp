//
// Created by marco on 02/05/19.
//

#ifndef TIMESERIESCLUSTERING_SPECTRALDISTANCE_CPP
#define TIMESERIESCLUSTERING_SPECTRALDISTANCE_CPP

#include "SpectralDistance.h"


SpectralDistance::SpectralDistance(){
    input_buffer1 = static_cast<double*      >(fftw_malloc(input_size  * sizeof(double)));
    output_buffer1 = static_cast<fftw_complex*>(fftw_malloc(output_size * sizeof(fftw_complex)));
    plan1 = fftw_plan_dft_r2c_1d(input_size, input_buffer1, output_buffer1, flags);

    input_buffer2 = static_cast<double*      >(fftw_malloc(input_size  * sizeof(double)));
    output_buffer2 = static_cast<fftw_complex*>(fftw_malloc(output_size * sizeof(fftw_complex)));
    plan2 = fftw_plan_dft_r2c_1d(input_size, input_buffer2, output_buffer2, flags);
}

arma::mat SpectralDistance::fourierFeatures(arma::mat data){
    int nDim = data.n_rows/100;
    arma::mat fourierMat;

    for(int k=0; k<data.n_cols; k++){
        arma::colvec currentCol = data.col(k);
        arma::colvec colToInsert(nDim*output_size);

        for(int j=0; j<nDim; j++) {
            for (int i=0; i<input_size; i++) {
                input_buffer1[i] = currentCol.at((j*input_size)+i);
            }

            fftw_execute(plan1);

            for(int i=0; i<output_size; i++){
                colToInsert[j*output_size + i] = output_buffer1[i][0];
            }
        }

        double tmpLen = colToInsert.n_elem;

        fourierMat.insert_cols(k, colToInsert);
    }


    /*
    fourierMat.for_each([](double& val){
        val = pow(val, 2)/(2*M_PI*100);
    });
    */
    return fourierMat;
}

double SpectralDistance::Evaluate(arma::vec a, arma::vec b) {
    double squaredSum = 0;
    int nDim = a.size()/100;
    for(int j=0; j<nDim; j++) {
        for (int i = 0; i < input_size; i++) {
            input_buffer1[i] = a.at((j*100)+i);
            input_buffer2[i] = b.at((j*100)+i);
        }

        fftw_execute(plan1);
        fftw_execute(plan2);

        for (int i = 0; i < output_size; i++) {
            squaredSum += pow((output_buffer1[i][0] - output_buffer2[i][0]), 2);
        }
    }

    return sqrt(squaredSum);
}

#endif //TIMESERIESCLUSTERING_SPECTRALDISTANCE_CPP
//
// Created by marco on 14/05/19.
//

#include "scoreFunctions.h"


double evalSilhouette(arma::Row<size_t> assignments, int index, size_t n_cluster, arma::mat data){
    std::vector<double> silhouettes(n_cluster,0);
    std::vector<int> silhouettes_n(n_cluster,0);


    size_t index_cluster = assignments[index];

    int count = 0;
    data.each_col([&count, &silhouettes, &silhouettes_n,index,assignments,data](arma::colvec col){
        if(count == index){
            count++;
            return;
        }

        size_t tmp_cluster = assignments[count];

        silhouettes[tmp_cluster] += mlpack::metric::EuclideanDistance::Evaluate(col, data.col(index));
        silhouettes_n[tmp_cluster]++;

        count++;
    });

    //Cluster di cardinalità 1 hanno silhouette 0
    if(silhouettes_n[index_cluster] == 0)
        return 0;

    for(int i=0; i<n_cluster; i++){
        silhouettes[i] /= silhouettes_n[i];
    }

    double a = silhouettes[index_cluster];

    double b = std::numeric_limits<double>::max();
    for(int i=0; i<n_cluster; i++){
        if(i == index_cluster)
            continue;
        b = std::min(b, silhouettes[i]);
    }


    return (b-a)/(std::max(a,b));
}

double evalSilhouette(arma::Row<size_t> assignments, int index, size_t n_cluster, arma::mat data, arma::mat precomputedDistances){
    std::vector<double> silhouettes(n_cluster,0);
    std::vector<int> silhouettes_n(n_cluster,0);

    size_t index_cluster = assignments[index];
    size_t tmp_cluster;
    for(int i=0; i<data.n_cols; i++){
        if(i == index)
            continue;

        tmp_cluster = assignments[i];

        silhouettes[tmp_cluster] += precomputedDistances[i, index];
        silhouettes_n[tmp_cluster]++;
    }


    //Cluster di cardinalità 1 hanno silhouette 0
    if(silhouettes_n[index_cluster] == 0)
        return 0;

    for(int i=0; i<n_cluster; i++){
        silhouettes[i] /= silhouettes_n[i];
    }

    double a = silhouettes[index_cluster];

    double b = std::numeric_limits<double>::max();
    for(int i=0; i<n_cluster; i++){
        if(i == index_cluster)
            continue;
        b = std::min(b, silhouettes[i]);
    }


    return (b-a)/(std::max(a,b));
}

double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data, arma::rowvec silhouettes){
    double s = 0;
    double tmp;

    for(int i=0; i<data.n_cols; i++){
        tmp = evalSilhouette(assignments, i, n_cluster, data);
        s += tmp;
        silhouettes[i] = tmp;
    }
    s /= data.n_cols;

    return s;
}

double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data){
    double s = 0;

    for(int i=0; i<data.n_cols; i++){
        s += evalSilhouette(assignments, i, n_cluster, data);
    }

    s /= data.n_cols;

    return s;
}

double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data, arma::mat precomputedDistances){
    double s = 0;

    for(int i=0; i<data.n_cols; i++){
        s += evalSilhouette(assignments, i, n_cluster, data, precomputedDistances);
    }

    s /= data.n_cols;

    return s;
}

/// spiegarlo DA FARE DA FARE DA FARE
/// \param subjectType vector containing subjects type
/// \param assignments vector containing cluster results
/// \return percentage
double controlNeuropathicSeparation(arma::Row<double> subjectType, arma::Row<size_t> assignments){
    int n_cluster = (int)assignments.max()+1;
    std::vector<int> iC;
    std::vector<int> iN;

    for(int i=0; i<assignments.size(); i++){
        switch ((int)subjectType[i]){
            case 0:iC.push_back(i);break;
            case 2:iN.push_back(i);break;
            default:;break;
        }
    }

    double sep = 0;
    for(int i=0; i<iC.size(); i++){
        for(int j=0; j<iN.size(); j++){
            if(assignments[i] == assignments[j]){
                sep += 1;
            }
        }
    }
    sep /= (iN.size() * iC.size());
    sep = 1-sep;

    sep *= fragmentationIndex(subjectType, assignments);

    return sep;
}

double controlNeuropathicSeparationDBSCAN(arma::Row<double> subjectType, arma::Row<size_t> assignments){
    //int n_cluster = (int)assignments.max();
    int n_cluster = 301;
    for(int i=0; i<assignments.n_elem; i++)
        if(assignments[i] >= 300)
            assignments[i] = 300;

    std::vector<int> iC;
    std::vector<int> iN;

    for(int i=0; i<assignments.size(); i++){
        switch ((int)subjectType[i]){
            case 0:iC.push_back(i);break;
            case 2:iN.push_back(i);break;
            default:;break;
        }
    }

    double sep = 0;
    for(int i=0; i<iC.size(); i++){
        for(int j=0; j<iN.size(); j++){
            if(assignments[i] == assignments[j]){
                sep += 1;
            }
        }
    }
    sep /= (iN.size() * iC.size());
    sep = 1-sep;

    sep *= fragmentationIndex(subjectType, assignments);

    return sep;
}


///
/// \param subjectType
/// \param assignments
/// \return
double fragmentationIndex(arma::Row<double> subjectType, arma::Row<size_t> assignments){
    int n_cluster = (int)assignments.max()+1;
    std::vector<int> iC(n_cluster, 0);
    std::vector<int> iN(n_cluster, 0);
    int countC = 0;
    int countN = 0;
    int maxC = 0;
    int maxN = 0;
    for(int i=0; i<assignments.size(); i++){
        switch ((int)subjectType[i]){
            case 0:
                iC[assignments[i]]++;
                if(iC[assignments[i]] > maxC){
                    maxC = iC[assignments[i]];
                }
                countC++;break;
            case 2:
                iN[assignments[i]]++;
                if(iN[assignments[i]] > maxN){
                    maxN = iN[assignments[i]];
                }
                countN++;break;
            default:;break;
        }
    }


    return ((double)maxC/countC)*((double)maxN/countN);
}

double fragmentationIndexDBSCAN(arma::Row<double> subjectType, arma::Row<size_t> assignments){
    int n_cluster = (int)assignments.max()+1;
    std::vector<int> iC(n_cluster, 0);
    std::vector<int> iN(n_cluster, 0);
    int countC = 0;
    int countN = 0;
    int maxC = 0;
    int maxN = 0;
    for(int i=0; i<assignments.size(); i++){
        switch ((int)subjectType[i]){
            case 0:
                iC[assignments[i]]++;
                if(iC[assignments[i]] > maxC){
                    maxC = iC[assignments[i]];
                }
                countC++;break;
            case 2:
                iN[assignments[i]]++;
                if(iN[assignments[i]] > maxN){
                    maxN = iN[assignments[i]];
                }
                countN++;break;
            default:;break;
        }
    }


    return ((double)maxC/countC)*((double)maxN/countN);
}
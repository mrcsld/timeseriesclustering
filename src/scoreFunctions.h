//
// Created by marco on 14/05/19.
//

#ifndef TIMESERIESCLUSTERING_SCOREFUNCTIONS_H
#define TIMESERIESCLUSTERING_SCOREFUNCTIONS_H

#include <mlpack/methods/kmeans/kmeans.hpp>


double evalSilhouette(arma::Row<size_t> assignments, int index, size_t n_cluster, arma::mat data, arma::mat precomputedDistances);
double evalSilhouette(arma::Row<size_t> assignments, int index, size_t n_cluster, arma::mat data);
double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data, arma::rowvec& silhouettes);
double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data, arma::mat precomputedDistances);
double silhouetteScore(size_t n_cluster, arma::Row<size_t> assignments, arma::mat data);
double controlNeuropathicSeparation(arma::Row<double> subjectType, arma::Row<size_t> assignments);
double controlNeuropathicSeparationDBSCAN(arma::Row<double> subjectType, arma::Row<size_t> assignments);
double fragmentationIndex(arma::Row<double> subjectType, arma::Row<size_t> assignments);

#endif //TIMESERIESCLUSTERING_SCOREFUNCTIONS_H

//
// Created by marco on 15/05/19.
//
#ifndef TIMESERIESCLUSTERING_DATATRANSFORM_H
#define TIMESERIESCLUSTERING_DATATRANSFORM_H


#include <mlpack/methods/kmeans/kmeans.hpp>
#include "loadMethods.h"
#include <math.h>
#include <string>


arma::mat averageFoots(arma::mat m);
arma::mat averageFootsRevised(arma::mat m);
arma::mat averageFootsRevised1(arma::mat m);
arma::mat dropRows(arma::mat data, std::vector<std::string>& labels, arma::rowvec& subjectType, std::vector<int> toDrop);
arma::mat newDataExtractor(std::vector<std::string>& labels, arma::rowvec& subjectType, std::string filename);
arma::mat newDataExtractor1(arma::mat m);
arma::mat newDataExtractor1(arma::mat m, arma::rowvec& subjectsType, std::vector<std::string>& labels);
arma::mat newDataExtractor2(arma::mat m);
arma::mat newDataExtractor2(arma::mat m, arma::rowvec& subjectsType, std::vector<std::string>& labels);
arma::mat averageSameSubject(arma::mat data, std::vector<std::string>& labels, arma::rowvec& subjectType);

#endif //TIMESERIESCLUSTERING_DATATRANSFORM_H

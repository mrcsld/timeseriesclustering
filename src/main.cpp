#include <iostream>
#include <string>
#include <time.h>
#include <math.h>

#include <fftw3.h>
#include <mlpack/methods/kmeans/kmeans.hpp>
#include <mlpack/methods/dbscan/dbscan.hpp>
#include <sys/stat.h>


#include "KMeanspp_initialization.cpp"
#include "loadMethods.h"
#include "printMethods.h"
#include "cleaningMethods.h"
#include "SpectralDistance.h"
#include "scoreFunctions.h"
#include "dataTransform.h"


#define STREAM(s) (((std::ostringstream&)(std::ostringstream() << s)).str())

using namespace std;


int method_1();
int method_2(int num_clust);
int method_3(int num_clust);
int method_4(int onePerRow, int centering, int normalize, int impute, int repeat, int score, int distance);
int method_5(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks);
int method_6(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks);

arma::mat readAndProcess(int onePerRow, int centering, int normalize, int impute, int distance, bool averageFoot, bool averageWalks, std::vector<int> toDrop, arma::rowvec& subjectsType, vector<string>& labels);
arma::mat preprocess(arma::mat& data, int centering, int normalize, int impute);
void clusterMethod(arma::mat data, arma::rowvec subjectsType, std::vector<string> labels, int repeat, int distance, int score, string dirname);
void clusterMethodDBSCAN(arma::mat data, arma::rowvec subjectsType, std::vector<string> labels, int repeat, int distance, int score, string dirname);
string createDirectory(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks);

int main(){
    int onePerRow = 1;  // 0-> 1PerRow, 1-> 100PerRow
    int centering = 1;  // 0-> no centering, 1->by mean
    int normalize = 1;  // 0-> no normalize, 1->by span, 2->by std
    int impute = 1;     // 0-> no impute, 1-> impute by avg (ONLY 1)
    int repeat = 100;   // number of rep
    int score = 1;      // 0-> by silhouette, 1-> by separation
    int distance = 0;   // 0-> euclidean, 1-> spectral
    bool averageFoots = false;
    bool averageWalks = false;

    //method_5(onePerRow,centering, normalize, impute, repeat, distance, score, averageFoots, 0);
    //method_6(onePerRow, centering, normalize, impute, repeat, distance, score, averageFoots, averageWalks);
    // A
    centering = 0;
    normalize = 0;
    averageFoots = false;
    averageWalks = false;

    //method_5(1, centering, normalize, impute, 10, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, 10, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 1, 1, averageFoots, averageWalks);

    // B
    centering = 1;
    normalize = 1;
    averageFoots = false;
    averageWalks = false;

    //method_5(1, centering, normalize, impute, 10, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, 10, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 1, 1, averageFoots, averageWalks);

    // C
    centering = 1;
    normalize = 2;
    averageFoots = false;
    averageWalks = false;

    //method_5(1, centering, normalize, impute, 10, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, 10, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 1, 1, averageFoots, averageWalks);

    // D
    centering = 1;
    normalize = 1;
    averageFoots = true;
    averageWalks = false;


    //method_5(1, centering, normalize, impute, repeat, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, repeat, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 1, 1, averageFoots, averageWalks);

    // E
    centering = 1;
    normalize = 2;
    averageFoots = true;
    averageWalks = false;

    //method_5(1, centering, normalize, impute, repeat, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, repeat, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 1, 1, averageFoots, averageWalks);


    // F
    centering = 1;
    normalize = 1;
    averageFoots = true;
    averageWalks = true;

    method_5(1, centering, normalize, impute, repeat, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, repeat, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, repeat, 1, 1, averageFoots, averageWalks);


    // G
    centering = 1;
    normalize = 2;
    averageFoots = true;
    averageWalks = true;

    //method_5(1, centering, normalize, impute, 10, 0, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 0, 1, averageFoots, averageWalks);

    //method_5(1, centering, normalize, impute, 10, 1, 0, averageFoots, averageWalks);
    //method_5(1, centering, normalize, impute, 10, 1, 1, averageFoots, averageWalks);


    // DBSCAN - TEST
    centering = 1;
    normalize = 1;
    distance = 1;
    score = 1;
    //method_5(onePerRow, centering, normalize, impute, repeat, distance, score, averageFoots, averageWalks);
    //method_6(onePerRow, centering, normalize, impute, repeat, distance, score, averageFoots, averageWalks);

    return 1;

    int count = 0;


    for(int i1 = 0; i1<2; i1++){//centering
        for(int i2 = 0; i2<3; i2++){//normalize
            for(int i3 = 0; i3<2; i3++){//score
                for(int i4 = 0; i4<2; i4++){//distance
                    method_5(onePerRow, i1, i2, impute, repeat, i4, i3, averageFoots, averageWalks);
                    count++;
                }
            }
        }
    }

    // miss -> 1perRow

    return 0;
}

int method_1(){
    std::time_t start;
    std::time_t end;

    //The dataset we are clustering.
    arma::mat data;

    //mlpack::data::Load("../data.csv", data);
    mlpack::data::Load("../data/test.csv", data);

    vector<string> labels = readCSVLabels("../data/test.csv");
    printLabels(labels);

    data.t().print();

    data.shed_col(0); // cancels header
    data.shed_rows(0,1); // cancels columns (0 and 1)
    data.t().print();

    data = removeAvg(data);
    data = removeStd(data);




    cout<<"\n AFTER REMOVE\n";
    data.t().print();
    cout<<"\n\n\n\n";
    data = substituteNaN(data);

    data.t().print();

    // The number of clusters we are getting.
    size_t n_cluster = 2;

    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;

    mlpack::kmeans::KMeanspp_initialization plusplus;
    mlpack::metric::EuclideanDistance distance;
    // Initialize with the default arguments.
    mlpack::kmeans::KMeans<mlpack::metric::EuclideanDistance, mlpack::kmeans::KMeanspp_initialization> k(1000, distance, plusplus);
    //k.Partitioner() mlpack::kmeans::KMeanspp_initialization();
    start = std::time(nullptr);
    k.Cluster(data, n_cluster, assignments);
    end = std::time(nullptr);
    std::cout<<end-start;
    std::cout<<"\n";

    start = std::time(nullptr);
    //cout<<silhouetteScore(n_cluster, assignments, data);
    std::cout<<"\n";
    end = std::time(nullptr);
    std::cout<<end-start;
    std::cout<<"\n";

    mlpack::data::Save("../assignments.csv", assignments);
    //mlpack::data::Save("../assignments.csv", arma::Row<int>(modifiedResults));

    return 0;
}

int method_2(int num_clust) {
    std::time_t start;
    std::time_t end;

    //The dataset we are clustering.
    arma::mat data;

    mlpack::data::Load("../data/oneRow.csv", data);
    //mlpack::data::Load("../data/test.csv", data);

    vector<string> labels = readCSVLabels("../data/oneRow.csv");
    //printLabels(labels);

    //data.shed_col(0); // cancels header
    data.shed_rows(0, 1); // cancels columns (0 and 1)

    //data = data.rows(2600,4999);
    cout << data.n_rows;


    //data = newDataExtractor();
    data = averageFoots(data);
    data = oneRowRemoveAvg(data); // riattiva
    //data = oneRowRemoveStd(data); //riattiva
    //data = oneRowNormalize(data);
    data = oneRowNormalize2(data);
    cout << "\t here: " << data.n_cols << "-" << data.n_rows;
    data.shed_cols(71, 72);
    data.shed_col(67);


    //data = oneRowSubstituteNaN(data); // riattiva
    //data = oneRowRemoveAvg(data);
    //data = oneRowNormalize2(data);
    //data = oneRowRemoveStd(data);

    mlpack::data::Save("../data/forPy1.csv", data);


    std::cout << "\t" << data.n_rows << "\t" << data.n_cols << "\t";

    //data = newDataExtractor1(data);

    //data.shed_rows(50,55);
    //data = oneRowRemoveStd(data); // riattiva
    //data.t().print();
    cout << "Numero elementi: " << data.n_cols;
    mlpack::data::Save("../data/forPy.csv", data);
    // The number of clusters we are getting.
    size_t n_cluster = num_clust;
    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    mlpack::kmeans::KMeanspp_initialization plusplus;
    SpectralDistance euclideanDistance;
    // Initialize with the default arguments.

/*
    arma::mat dMatrix = arma::eye(data.n_cols, data.n_cols);
    for(int i = 0; i<data.n_cols-1; i++){
        for(int j = i; j<data.n_cols; j++){
            double dist = euclideanDistance.Evaluate(data.col(i), data.col(j));
            dMatrix.at(i, j) = dist;
            dMatrix.at(j, i) = dist;
        }
    }
    mlpack::data::Save("../data/dist_matrix.csv", dMatrix);
*/

    mlpack::kmeans::KMeans<SpectralDistance, mlpack::kmeans::KMeanspp_initialization> k(10000000, euclideanDistance,
                                                                                        plusplus);

    int repeat = 20;
    for (int i = 2; i < 60; i++) {
        start = std::time(nullptr);
        n_cluster = i;
        arma::Row<size_t> tmpAssignments;
        double bestSilhouette = std::numeric_limits<double>::min();

        for (int r = 0; r < repeat; r++) {
            cout << "ACTUAL IT: " << r << "\n";
            k.Cluster(data, n_cluster, tmpAssignments);
            double tmp_sil = silhouetteScore(n_cluster, tmpAssignments, data);
            if (max(tmp_sil, bestSilhouette) == tmp_sil) {
                bestSilhouette = tmp_sil;
                assignments = tmpAssignments;
            }
        }

        cout << "Silhouette value for " << n_cluster << " clusters: " << bestSilhouette << "\t";
        mlpack::data::Save("../assignments.csv", assignments);

        vector<int> resultsCheck(i, 0);
        for (int j = 0; j < 62; j++) {
            resultsCheck[assignments.at(j)] += 1;
        }

        for (int j = 0; j < i; j++) {
            if (((double) (resultsCheck.at(j)) / 62) > 0.5) {
                mlpack::data::Save(STREAM("../data/results_" << i << "_clusters.csv"), assignments);
            }
        }
    }

    return 0;
}


int method_3(int num_clust){
    std::time_t start;
    std::time_t end;

    //The dataset we are clustering.
    arma::mat data;

    mlpack::data::Load("../data/oneRow.csv", data);
    //mlpack::data::Load("../data/test.csv", data);

    vector<string> labels = readCSVLabels("../data/oneRow.csv");
    //printLabels(labels);

    //data.shed_col(0); // cancels header
    data.shed_rows(0,1); // cancels columns (0 and 1)

    // 0-2600-5000-5600(-1)
    //data.shed_rows(0,2599); // angles
    //  data.shed_rows(2600, 5599); // torques
    //data = data.rows(601,700); // forces
    cout<<"actual:"<<data.n_rows<<"\n";
    data.shed_rows(1300,2599);
    cout<<"actual:"<<data.n_rows<<"\n";
    data.shed_rows(2500,3699);
    cout<<"actual:"<<data.n_rows<<"\n";
    data.shed_rows(2800,3099);
    cout<<"actual:"<<data.n_rows<<"\n";


    data = oneRowRemoveAvg(data);
    data = oneRowRemoveStd(data);
    //data = oneRowNormalize(data);
    //data = oneRowNormalize2(data);


    data = oneRowSubstituteNaN(data);
    data = oneRowRemoveStd(data);
    //data.t().print();
    cout<<"Numero elementi: "<<data.n_cols;
    mlpack::data::Save("../data/forPy.csv", data);
    // The number of clusters we are getting.
    size_t n_cluster = num_clust;
    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    mlpack::kmeans::KMeanspp_initialization plusplus;
    SpectralDistance sd;
    // Initifalize with the default arguments.
    mlpack::kmeans::KMeans<SpectralDistance, mlpack::kmeans::KMeanspp_initialization> k(10000000, sd, plusplus);

    for(int i=2; i<60;i++){
        start = std::time(nullptr);
        n_cluster = i;
        k.Cluster(data, n_cluster, assignments);
        end = std::time(nullptr);
        start = std::time(nullptr);
        cout<<"Silhouette value for "<<n_cluster<<" clusters: "<< silhouetteScore(n_cluster, assignments, data)<<"\t";
        end = std::time(nullptr);
        std::cout<<"Silhouette evaluation time:"<<end-start<<"\n";
        mlpack::data::Save("../assignments.csv", assignments);
        vector<int> resultsCheck(i, 0);
        for(int j=0; j<62; j++) {
            resultsCheck[assignments.at(j)] += 1;
        }
        for(int j=0; j<i; j++){
            if(((double)(resultsCheck.at(j))/62) > 0.75){
                mlpack::data::Save(STREAM("../data/results_"<<i<<"_clusters.csv"), assignments);
            }
        }
    }

    return 0;
}


int method_4(int onePerRow, int centering, int normalize, int impute, int repeat, int score, int distance){
    std::time_t start;
    std::time_t end;

    //The dataset we are clustering.
    arma::mat data;

    vector<string> labels;
    arma::rowvec subjectsType;

    data = newDataExtractor(labels, subjectsType, "../data/oneRow.csv");
    data = averageSameSubject(data, labels, subjectsType);

    data = averageFootsRevised(data);


    std::vector<int> toDrop{72, 71, 67};
    data = dropRows(data, labels, subjectsType, toDrop);


    mlpack::data::Save("../data/forPy.csv", data);


    data = preprocess(data, centering, normalize, impute);



    mlpack::data::Save("../data/forPy1.csv", data);


    std::cout << "\t" << data.n_rows << "\t" << data.n_cols << "\t";

    cout << "\n Numero elementi: " << data.n_cols;
    // The number of clusters we are getting.
    size_t n_cluster = 3;
    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    mlpack::kmeans::KMeanspp_initialization plusplus;
    SpectralDistance euclideanDistance;
    // Initialize with the default arguments.

    std::cout << "\n\t" << data.n_rows << "\t" << data.n_cols << "\t";
    data = euclideanDistance.fourierFeatures(data);
    std::cout << "\n\t" << data.n_rows << "\t" << data.n_cols << "\t";
    mlpack::metric::EuclideanDistance ed;

    mlpack::kmeans::KMeans<mlpack::metric::EuclideanDistance, mlpack::kmeans::KMeanspp_initialization> k(10000000, ed, plusplus);

    arma::rowvec silhouettePlot(78 - 2);
    arma::rowvec separationPlot(20 - 2);
    //repeat = 20;
    for (int i = 2; i < 20; i++) {
        start = std::time(nullptr);
        n_cluster = i;
        arma::Row<size_t> tmpAssignments(i);
        double bestSilhouette = std::numeric_limits<double>::min();
        double bestSeparation = std::numeric_limits<double>::min();

        for (int r = 0; r < repeat; r++) {
            cout << "ACTUAL IT: " << r << "\n";
            arma::mat oldCentroids;
            arma::mat newCentroids;
            k.Cluster(data, n_cluster, tmpAssignments, oldCentroids);
            //tmpAssignments.print();
            double tmp_sil = silhouetteScore(n_cluster, tmpAssignments, data);
            if (max(tmp_sil, bestSilhouette) == tmp_sil) {
                bestSilhouette = tmp_sil;
                //assignments = tmpAssignments;
            }

            double tmpSep = controlNeuropathicSeparation(subjectsType, tmpAssignments);
            if (max(tmpSep, bestSeparation) == tmpSep) {
                bestSeparation = tmpSep;
                assignments = tmpAssignments;
            }
        }



        silhouettePlot[i-2] = bestSilhouette;
        separationPlot[i-2] = bestSeparation;

        cout << "Silhouette value for " << n_cluster << " clusters: " << bestSilhouette << "\t";
        cout << "Separation value for " << n_cluster << " clusters: " << bestSeparation << "\t";
        //mlpack::data::Save("../data/spectral/assignments.csv", assignments);

        vector<int> resultsCheck(i, 0);
        for (int j = 0; j < 21; j++) {
            resultsCheck[assignments.at(j)] += 1;
        }

        for (int j = 0; j < i; j++) {
            if (((double) (resultsCheck.at(j)) / 21) > 0.5) {
                mlpack::data::Save(STREAM("../data/spectral/results_" << i << "_clusters.csv"), assignments);
            }
        }
    }

    mlpack::data::Save("../data/spectral/silhouettes1.csv", silhouettePlot);
    mlpack::data::Save("../data/spectral/separation.csv", separationPlot);

    return 0;
}

int method_5(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks){
    string dirname = createDirectory(onePerRow, centering, normalize, impute, repeat, distance, score, averageFoots, averageWalks);

    //std::vector<int> toDrop{72, 71, 67};
    arma::rowvec subjectsType;
    std::vector<string> labels;
    std::vector<int> toDrop;


    arma::mat data = readAndProcess(onePerRow, centering, normalize, impute, distance, averageFoots, averageWalks, toDrop, subjectsType, labels);

    clusterMethod(data, subjectsType, labels, repeat, distance, score, dirname);

    return 0;
}

int method_6(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks){
    //string dirname = createDirectory(onePerRow, centering, normalize, impute, repeat, distance, score, averageFoots, averageWalks);
    string dirname = "../data/DBSCAN_TEST/";
    //std::vector<int> toDrop{72, 71, 67};
    arma::rowvec subjectsType;
    std::vector<string> labels;
    std::vector<int> toDrop;


    arma::mat data = readAndProcess(onePerRow, centering, normalize, impute, distance, averageFoots, averageWalks, toDrop, subjectsType, labels);

    clusterMethodDBSCAN(data, subjectsType, labels, repeat, distance, score, dirname);

    return 0;
}


void clusterMethod(arma::mat data, arma::rowvec subjectsType, std::vector<string> labels, int repeat, int distance, int score, string dirname){
    size_t n_cluster;

    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    // K-Means++ init
    mlpack::kmeans::KMeanspp_initialization plusplus;


    if(distance == 1){
        SpectralDistance sd;
        data = sd.fourierFeatures(data);
    }

    mlpack::metric::EuclideanDistance ed;
    mlpack::kmeans::KMeans<mlpack::metric::EuclideanDistance, mlpack::kmeans::KMeanspp_initialization> k(10000000, ed, plusplus);

    int min_n_cluster = 2;
    ls
    int max_n_cluster = 10; //TODO rimettere a 20

    arma::rowvec silhouettePlot(max_n_cluster - min_n_cluster);
    arma::rowvec separationPlot(max_n_cluster - min_n_cluster);
    //int repeat = 20;
    arma::Row<size_t> tmpAssignments;
    double bestSilhouette;
    double bestSeparation;

    for (int i = min_n_cluster; i < max_n_cluster+1; i++) {
        n_cluster = i;
        tmpAssignments = arma::Row<size_t>(i);
        bestSilhouette = std::numeric_limits<double>::min();
        bestSeparation = std::numeric_limits<double>::min();
        cout<<"\n";
        for (int r = 0; r < repeat; r++) {
            cout << "ACTUAL IT: " << r << "\n";
            k.Cluster(data, n_cluster, tmpAssignments);
            //tmpAssignments.print();
            switch(score) {
                case 0: {
                    double tmp_sil = silhouetteScore(n_cluster, tmpAssignments, data);
                    if (max(tmp_sil, bestSilhouette) == tmp_sil) {
                        bestSilhouette = tmp_sil;
                        assignments = tmpAssignments;
                    };
                    break;
                }
                case 1: {
                    double tmpSep = controlNeuropathicSeparation(subjectsType, tmpAssignments);
                    if (max(tmpSep, bestSeparation) == tmpSep) {
                        bestSeparation = tmpSep;
                        assignments = tmpAssignments;
                    };
                    break;
                }
                default: {
                    break;
                }
            }
        }

        switch(score){
            case 0: {
                silhouettePlot[i-2] = bestSilhouette;
                cout << "Silhouette value for " << n_cluster << " clusters: " << bestSilhouette << "\t";
                break;
            }

            case 1: {
                separationPlot[i-2] = bestSeparation;
                cout << "Separation value for " << n_cluster << " clusters: " << bestSeparation << "\t";
                break;
            }

            default: {
                break;
            }
        }


        mlpack::data::Save(STREAM(dirname << "results_" << i << "_clusters.csv"), assignments);


    }
    if(score == 0)
        mlpack::data::Save(STREAM(dirname << "silhouettes.csv"), silhouettePlot);
    else if(score == 1)
        mlpack::data::Save(STREAM(dirname << "separation.csv"), separationPlot);
}

void clusterMethodDBSCAN(arma::mat data, arma::rowvec subjectsType, std::vector<string> labels, int repeat, int distance, int score, string dirname){
    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    dirname = "../data/DBSCAN_TEST/";
    if(distance == 1){
        SpectralDistance sd;
        data = sd.fourierFeatures(data);
    }

    mlpack::data::Save(STREAM(dirname << "forDistanceCheck.csv"), data);

    double minEpsilon = 3;
    double maxEpsilon = 3.5;
    double epsilon = 3.175;
    int minPts = 1;

    int n_trials = 51;
    arma::rowvec separationPlot(n_trials);
    arma::Row<size_t> tmpAssignments;
    double bestSeparation;

    repeat = 1;



    if(distance == 0){
        double minEpsilon = 3;

        for(int j=20; j<40; j++){
            bestSeparation = std::numeric_limits<double>::min();
            for (int i = 0; i < n_trials; i++) {
                cout<<"\n";
                mlpack::dbscan::DBSCAN<mlpack::range::RangeSearch<>, mlpack::dbscan::RandomPointSelection> dbs(minEpsilon+0.01*i, j);
                for (int r = 0; r < repeat; r++) {
                    cout << "ACTUAL IT: " << r << "\n";
                    dbs.Cluster(data, tmpAssignments);

                    for(int i=0; i<tmpAssignments.n_elem; i++)
                        if(tmpAssignments[i] >= 300)
                            tmpAssignments[i] = 300;

                    double tmpSep = controlNeuropathicSeparationDBSCAN(subjectsType, tmpAssignments);

                    if (max(tmpSep, bestSeparation) == tmpSep) {
                        bestSeparation = tmpSep;
                        assignments = tmpAssignments;
                    }
                }

                separationPlot[i] = bestSeparation;
                cout << "Separation value for epsilon=" << (minEpsilon+0.01*i) << ", minPts="<<j<<":" << bestSeparation << "\t";




            }


            mlpack::data::Save(STREAM(dirname << "euclidean_results_" << j << "_minPts.csv"), assignments);
            mlpack::data::Save(STREAM(dirname << "euclidean_separation_"<< j <<"minPts.csv"), separationPlot);
        }
    } else if(distance == 1){
        double minEpsilon = 13;
        for(int j=1; j<8; j++){
            bestSeparation = std::numeric_limits<double>::min();
            for (int i = 0; i < n_trials; i++) {
                cout<<"\n";
                mlpack::dbscan::DBSCAN<mlpack::range::RangeSearch<>, mlpack::dbscan::RandomPointSelection> dbs(minEpsilon+0.01*i, j);
                for (int r = 0; r < repeat; r++) {
                    cout << "ACTUAL IT: " << r << "\n";
                    dbs.Cluster(data, tmpAssignments);

                    for(int i=0; i<tmpAssignments.n_elem; i++)
                        if(tmpAssignments[i] >= 300)
                            tmpAssignments[i] = 300;

                    double tmpSep = controlNeuropathicSeparationDBSCAN(subjectsType, tmpAssignments);

                    if (max(tmpSep, bestSeparation) == tmpSep) {
                        bestSeparation = tmpSep;
                        assignments = tmpAssignments;
                    }
                }

                separationPlot[i] = bestSeparation;
                cout << "Separation value for epsilon=" << (minEpsilon+0.04*i) << ", minPts="<<j<<":" << bestSeparation << "\t";




            }

            mlpack::data::Save(STREAM(dirname << "spectral_results_" << j << "_minPts.csv"), assignments);
            mlpack::data::Save(STREAM(dirname << "spectral_separation_"<< j <<"minPts.csv"), separationPlot);
        }
    }



}

void clusterMethodPrecomputed(arma::mat data, arma::rowvec subjectsType, std::vector<string> labels, int repeat, int distance, int score, string dirname){
    size_t n_cluster;

    // The assignments will be stored in this vector.
    arma::Row<size_t> assignments;
    // K-Means++ init
    mlpack::kmeans::KMeanspp_initialization plusplus;


    if(distance == 1){
        SpectralDistance sd;
        data = sd.fourierFeatures(data);
    }


    mlpack::metric::EuclideanDistance ed;

    arma::mat precomputedDistances(data.n_cols, data.n_cols);
    for(int i=0; i<data.n_cols; i++){
        for(int j=0; j<data.n_cols; j++){
            precomputedDistances[i, j] = ed.Evaluate(data.col(i), data.col(j));
            precomputedDistances[j, i] = precomputedDistances[i, j];
        }
    }
    mlpack::kmeans::KMeans<mlpack::metric::EuclideanDistance, mlpack::kmeans::KMeanspp_initialization> k(10000000, ed, plusplus);

    int min_n_cluster = 2;
    int max_n_cluster = 20;

    arma::rowvec silhouettePlot(max_n_cluster - min_n_cluster);
    arma::rowvec separationPlot(max_n_cluster - min_n_cluster);
    //int repeat = 20;
    arma::Row<size_t> tmpAssignments;
    double bestSilhouette;
    double bestSeparation;

    for (int i = min_n_cluster; i < max_n_cluster+1; i++) {
        n_cluster = i;
        tmpAssignments = arma::Row<size_t>(i);
        bestSilhouette = std::numeric_limits<double>::min();
        bestSeparation = std::numeric_limits<double>::min();
        cout<<"\n";
        for (int r = 0; r < repeat; r++) {
            cout << "ACTUAL IT: " << r << "\n";
            k.Cluster(data, n_cluster, tmpAssignments);
            //tmpAssignments.print();
            switch(score) {
                case 0: {
                    double tmp_sil = silhouetteScore(n_cluster, tmpAssignments, data);
                    if (max(tmp_sil, bestSilhouette) == tmp_sil) {
                        bestSilhouette = tmp_sil;
                        assignments = tmpAssignments;
                    };
                    break;
                }
                case 1: {
                    double tmpSep = controlNeuropathicSeparation(subjectsType, tmpAssignments);
                    if (max(tmpSep, bestSeparation) == tmpSep) {
                        bestSeparation = tmpSep;
                        assignments = tmpAssignments;
                    };
                    break;
                }
                default: {
                    break;
                }
            }
        }

        switch(score){
            case 0: {
                silhouettePlot[i-2] = bestSilhouette;
                cout << "Silhouette value for " << n_cluster << " clusters: " << bestSilhouette << "\t";
                break;
            }

            case 1: {
                separationPlot[i-2] = bestSeparation;
                cout << "Separation value for " << n_cluster << " clusters: " << bestSeparation << "\t";
                break;
            }

            default: {
                break;
            }
        }


        mlpack::data::Save(STREAM(dirname << "results_" << i << "_clusters.csv"), assignments);


    }
    if(score == 0)
        mlpack::data::Save(STREAM(dirname << "silhouettes.csv"), silhouettePlot);
    else if(score == 1)
        mlpack::data::Save(STREAM(dirname << "separation.csv"), separationPlot);
}

arma::mat readAndProcess(int onePerRow, int centering, int normalize, int impute, int distance, bool averageFoot, bool averageWalks, std::vector<int> toDrop, arma::rowvec& subjectsType, vector<string>& labels){
    //The dataset we are clustering.
    arma::mat data;

    data = newDataExtractor(labels, subjectsType, "../data/oneRow_refined.csv");


    std::cout<<"Before:"<<data.size()<<"\t";
    if(averageWalks)
        data = averageSameSubject(data, labels, subjectsType);

    printLabels(labels);
    std::cout<<"BEFORE_SUB"<<subjectsType.n_elem<<"\t";


    cout<<"MIDDLE:"<<labels.size()<<"\t";

    if(averageFoot)
        data = averageFootsRevised(data); //TESI_Rimettere Revised

    std::sort(toDrop.begin(), toDrop.end(), greater<int>());



    data = dropRows(data, labels, subjectsType, toDrop);

    std::cout<<"After:"<<data.size()<<"\t";


    mlpack::data::Save("../data/beforeProcessing.csv", data);


    data = preprocess(data, centering, normalize, impute);

    mlpack::data::Save("../data/afterProcessing.csv", data);

    return data;
}

///
/// \param data dataset to preprocess
/// \param centering 0->No centering, 1->Center by average
/// \param normalize 0->No normalization, 1->Center by span, 2->Center by std
/// \param impute 0->Substitute with 0, 1->Substitute with mean
arma::mat preprocess(arma::Mat<double>& data, int centering, int normalize, int impute){
    std::cout<<"\nCENTERING";
    switch(centering){
        case 0:;break;
        case 1:data = oneRowRemoveAvg(data);break;
        default:std::cout<<"\nERR::Wrong centering value:"<<centering<<"\n";break;
    }

    std::cout<<"\nSTANDARDIZATION";
    switch(normalize){
        case 0:;break;
        case 1:data = oneRowNormalize2(data);break;
        case 2:data = oneRowStandardize(data);break;
        default:std::cout<<"\nERR::Wrong normalization value:"<<normalize<<"\n";break;
    }

    std::cout<<"\nIMPUTE";
    switch(impute){
        case 0:;break;
        case 1:data = oneRowSubstituteNaN(data);break;
        default:std::cout<<"\nERR::Wrong impute value:"<<impute<<"\n";break;
    }

    return data;
}



string createDirectory(int onePerRow, int centering, int normalize, int impute, int repeat, int distance, int score, bool averageFoots, bool averageWalks){
    string dirname = "../data/";


    switch(onePerRow) {
        case 0: {
            dirname.append("1PerRow_");
            break;
        }

        case 1: {
            dirname.append("100PerRow_");
            break;
        }

        default: {
            break;
        }
    }

    if(averageFoots){
        dirname.append("AVGFoots_");
    } else {
        dirname.append("2Foots_");
    }

    if(averageWalks){
        dirname.append("AVGWalks_");
    } else {
        dirname.append("SingleWalk_");
    }

    switch(centering) {
        case 0: {
            dirname.append("NoCentering_");
            break;
        }

        case 1: {
            dirname.append("MeanCentering_");
            break;
        }

        case 2: {
            dirname.append("MedianCentering_");
            break;
        }

        default: {
            break;
        }
    }

    switch(normalize) {
        case 0: {
            dirname.append("NoNormalization_");
            break;
        }

        case 1: {
            dirname.append("SpanNormalization_");
            break;
        }

        case 2: {
            dirname.append("VarianceNormalization_");
            break;
        }

        default: {
            break;
        }
    }

    switch(impute) {
        case 0: {
            dirname.append("NoImpute_");
            break;
        }

        case 1: {
            dirname.append("MeanImpute_");
            break;
        }

        case 2: {
            dirname.append("MedianImpute_");
            break;
        }

        default: {
            break;
        }
    }

    switch(distance) {
        case 0: {
            dirname.append("Euclidean_");
            break;
        }

        case 1: {
            dirname.append("Spectral_");
            break;
        }

        case 2: {
            break;
        }

        default: {
            break;
        }
    }

    switch(score) {
        case 0: {
            dirname.append("Silhouette_");
            break;
        }

        case 1: {
            dirname.append("CNSeparation_");
            break;
        }

        case 2: {
            break;
        }

        default: {
            break;
        }
    }

    dirname.append("/");

    mkdir(dirname.data(), 0777);

    return dirname;

}

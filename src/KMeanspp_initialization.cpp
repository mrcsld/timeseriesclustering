//
// Created by marco on 20/01/19.
//

#include <mlpack/methods/kmeans/sample_initialization.hpp>

#ifndef __MLPACK_METHODS_KMEANS_KMEANSPP_INITIALIZATION_HPP
#define __MLPACK_METHODS_KMEANS_KMEANSPP_INITIALIZATION_HPP

#include <mlpack/prereqs.hpp>
#include <mlpack/core/math/random.hpp>
#include <mlpack/core/metrics/lmetric.hpp>
#include <math.h>
#include <vector>

namespace mlpack {
namespace kmeans {
    class KMeanspp_initialization {
      public:
          KMeanspp_initialization(){ }


          /**
            * Initialize the centroids matrix by randomly sampling points from the data
            * matrix.
            *
            * @param data Dataset.
            * @param clusters Number of clusters.
            * @param centroids Matrix to put initial centroids into.
            */
          template<typename MatType>
          inline static void Cluster(const MatType& data,
                                     const size_t clusters,
                                     arma::mat& centroids)
          {
            metric::EuclideanDistance dist;
            std::mt19937 gen;
            gen.seed(time(0));

            // We create the matrix of initial centroids (n_rows*clusters), n_rows since the matrix is transposed
            centroids.set_size(data.n_rows, clusters);

            // We select the first point randomly
            size_t firstCentroidIndex = math::RandInt(0, data.n_cols);
            centroids.col(0) = data.col(firstCentroidIndex);

            double tmp_dist;
            std::vector<double> distances(data.n_cols);

            for(int i=1; i<clusters; i++){
              //Until we fill all the clusters
              for(int j=0; j<data.n_cols; j++){
                //Foreach element in our dataset
                tmp_dist = std::numeric_limits<double>::max();
                for(int l=0; l<i; l++){
                  //Foreach cluster already found we compute the distance and keep the minimum
                  tmp_dist = std::min(tmp_dist, dist.Evaluate(centroids.col(l),data.col(j)));
                }
                distances[j] = tmp_dist;
              }

              std::discrete_distribution<int> distribution(std::begin(distances), std::end(distances));
              centroids.col(i) = data.col(distribution(gen));
            }
          }
        };




} // namespace kmeans
} // namespace mlpack

#endif


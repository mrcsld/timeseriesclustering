//
// Created by marco on 18/04/19.
//

#include <iostream>
#include "loadMethods.h"

using namespace std;

vector<string> readCSVLabels(std::string filename){
    vector<string> labels;
    ifstream infile(filename);
    string line;
    while (getline(infile, line))
    {
        //istringstream iss(line);
        string token;
        long pos = line.find(',');
        //iss >> token;
        token = line.substr(0,pos);
        labels.push_back(token);
        // process pair (a,b)
    }

    return labels;
}
//
// Created by marco on 15/05/19.
//

#include "dataTransform.h"
#include "SpectralDistance.h"

/// Averages left and right foot data
/// \param m dataset
/// \return averaged dataset
arma::mat averageFoots(arma::mat m){
    arma::mat newMat;
    int insertionIndex = 0;

    for(int i=0; i<13; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+13)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count](double val){
                    if(!std::isnan(val)){
                        sum += val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    for(int i=26; i<38; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+12)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count](double val){
                    if(!std::isnan(val)){
                        sum += val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }


    for(int i=50; i<53; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+3)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count](double val){
                    if(!std::isnan(val)){
                        sum += val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    return newMat;
}

/// Averages left and right foot data
/// \param m dataset
/// \return averaged dataset
arma::mat averageFootsRevised(arma::mat m){
    arma::mat newMat;
    int insertionIndex = 0;
    std::vector<int> sign{-1,-1,1, 1,1,1, 1, 1,1,1, -1,-1,-1, 1,-1,-1, 1,1,-1, 1,1,1, 1,1,1, -1,1,1};

    for(int i=0; i<13; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+13)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count, sign, i](double val){
                    if(!std::isnan(val)){
                        sum += sign[i]*val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    for(int i=26; i<38; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+12)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count, sign, i](double val){
                    if(!std::isnan(val)){
                        sum += sign[i-26+13]*val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }


    for(int i=50; i<53; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+3)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;
                col.for_each([&sum, &count, sign, i](double val){
                    if(!std::isnan(val)){
                        sum += sign[i-50+25]*val;
                        count++;
                    }
                });

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    return newMat;
}


/// Averages left and right foot data
/// \param m dataset
/// \return averaged dataset
arma::mat averageFootsRevised1(arma::mat m){
    arma::mat newMat;
    int insertionIndex = 0;
    std::vector<int> sign{-1,-1,1, 1,1,1, 1, 1,1,1, -1,-1,-1, 1,-1,-1, 1,1,-1, 1,1,1, 1,1,1, -1,1,1};

    for(int i=0; i<13; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+13)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;
                if(!std::isnan(col.at(0))) {
                    sum += col.at(0);
                    count++;
                }
                if(!std::isnan(col.at(1))) {
                    sum += sign[i] * col.at(1);
                    count++;
                }

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    for(int i=26; i<38; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+12)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;

                if(!std::isnan(col.at(0))) {
                    sum += col.at(0);
                    count++;
                }
                if(!std::isnan(col.at(1))) {
                    sum += sign[i - 26 + 13] * col.at(1);
                    count++;
                }

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }


    for(int i=50; i<53; i++){
        for(int j=0; j<100; j++){
            arma::mat tmpMat = arma::zeros(2,m.n_cols);
            arma::rowvec toInsert(m.n_cols);

            tmpMat.insert_rows(0, m.row(i*100 + j));
            tmpMat.insert_rows(1, m.row((i+3)*100 + j));
            int index = 0;

            tmpMat.each_col([&index, &toInsert, sign, i](arma::colvec col){
                double sum = 0;
                int count = 0;

                if(!std::isnan(col.at(0))) {
                    sum += col.at(0);
                    count++;
                }
                if(!std::isnan(col.at(1))) {
                    sum += sign[i-50+25] * col.at(1);
                    count++;
                }

                toInsert[index++] = sum/count;
            });

            newMat.insert_rows(insertionIndex++, toInsert);
        }
    }

    return newMat;
}


/// Extracts data and places it in the proper structures
/// \param labels labels of the rows
/// \param subjectType vector containing the subject type
/// \param filename file to load
/// \return matrix containing the data
arma::mat newDataExtractor(std::vector<std::string>& labels, arma::rowvec& subjectType, std::string filename){
    arma::mat data;
    mlpack::data::Load(filename, data);

    labels = readCSVLabels(filename);
    subjectType = data.row(1);

    data.shed_rows(0,1); // cancels columns (0 and 1)

    return data;
}


/// changes data from 100-per-row to one-per-row
/// \param m data to be modified
/// \return modified data
arma::mat newDataExtractor1(arma::mat m){
    arma::mat newMat;
    int index = 0;

    m.each_col([&newMat, &index](arma::colvec col){
        int len = col.size();
        int nDim = len/100;
        for(int i=0; i<100; i++){
            arma::mat tmpCol = arma::zeros(nDim);
            for(int j=0; j<nDim; j++){
                tmpCol[j] = col[j*100+i];
            }
            newMat.insert_cols(index++, tmpCol);
            tmpCol.clear();
        }
    });

    return newMat;
}

/// changes data from 100-per-row to one-per-row
/// \param m data to be modified
/// \return modified data
arma::mat newDataExtractor1(arma::mat m, arma::rowvec& subjectsType, std::vector<std::string>& labels){
    arma::mat newMat;
    int index = 0;

    arma::rowvec tmpSubjectsType(subjectsType.size()*100);
    std::vector<std::string> tmpLabels(labels.size()*100, "");

    for(int i=0; i<subjectsType.size(); i++){
        for(int j=i*100; j<(i+1)*100; j++){
            tmpSubjectsType[j] = subjectsType[i];
        }
    }

    subjectsType = tmpSubjectsType;

    for(int i=0; i<labels.size(); i++){
        for(int j=i*100; j<(i+1)*100; j++){
            tmpLabels[j] = std::string(labels[i].data());
            (tmpLabels[j].append("_")).append(std::to_string(j%100));

        }
    }

    labels = tmpLabels;

    m.each_col([&newMat, &index](arma::colvec col){
        int len = col.size();
        int nDim = len/100;
        for(int i=0; i<100; i++){
            arma::mat tmpCol = arma::zeros(nDim);
            for(int j=0; j<nDim; j++){
                tmpCol[j] = col[j*100+i];
            }
            newMat.insert_cols(index++, tmpCol);
            tmpCol.clear();
        }
    });

    return newMat;
}

/// Changes data from 1-per-row to 100-per-row
/// \param m
/// \return
arma::mat newDataExtractor2(arma::mat m){
    arma::mat newMat(m.n_rows*100, m.n_cols/100);

    for(int i=0; i<m.n_cols; i++){
        for(int j=0; j<m.n_rows; j++){
            newMat[i/100,j*100+(i%100)] = m[i,j];
        }
    }

    return newMat;
}

/// Changes data from 1-per-row to 100-per-row
/// \param m
/// \return
arma::mat newDataExtractor2(arma::mat m, arma::rowvec& subjectsType, std::vector<std::string>& labels){
    arma::mat newMat(m.n_rows*100, m.n_cols/100);

    arma::rowvec tmpSubjectsType(subjectsType.size()/100);
    std::vector<std::string> tmpLabels(labels.size()/100, "");

    for(int i=0; i<subjectsType.size(); i+=100){
        tmpSubjectsType[i/100] = subjectsType[i];
    }

    subjectsType = tmpSubjectsType;

    for(int i=0; i<labels.size(); i+=100){
        tmpLabels[i/100] = std::string(labels[i].substr(0,labels[i].find_last_of("_")));
        std::cout<<labels[i];
        std::cout<<tmpLabels[i/100];

    }

    labels = tmpLabels;


    for(int i=0; i<m.n_cols; i++){
        for(int j=0; j<m.n_rows; j++){
            newMat[i/100,j*100+(i%100)] = m[i,j];
        }
    }

    return newMat;
}


arma::mat averageSameSubject(arma::mat data, std::vector<std::string>& labels, arma::rowvec& subjectType){

    arma::mat newData;
    arma::mat tmpData;
    int index = 0;
    int tmpIndex = 0;

    for(int i=0; i<labels.size(); i++){
        if(i==0 || labels[i-1].substr(0,labels[i-1].find_first_of('_')).compare(labels[i].substr(0,labels[i].find_first_of('_'))) == 0){
            tmpData.insert_cols(tmpIndex++,data.col(i));
        } else {
            arma::mat tmpCol = arma::zeros(data.n_rows);
            int j = 0;
            //tmpData = tmpData.each_row([&j, &tmpCol](arma::rowvec thisRow){
            tmpData.each_row([&j, &tmpCol](arma::rowvec thisRow){
                double sum = 0;
                int count = 0;

                thisRow.for_each([&sum, &count](double val){
                    if(!std::isnan(val)){
                        sum += val;
                        count++;
                    }
                });
                tmpCol[j++] = sum/count;
            });

            newData.insert_cols(index++, tmpCol);
            tmpData.clear();
            tmpCol.clear();
            tmpIndex = 0;
        }
    }

    if(tmpData.size() != 0){
        arma::mat tmpCol = arma::zeros(data.n_rows);
        int j = 0;
        tmpData = tmpData.each_row([&j, &tmpCol](arma::rowvec thisRow){
            double sum = 0;
            int count = 0;

            thisRow.for_each([&sum, &count](double val){
                if(!std::isnan(val)){
                    sum += val;
                    count++;
                }
            });
            tmpCol[j++] = sum/count;
        });

        newData.insert_cols(index++, tmpCol);
        tmpData.clear();
        tmpCol.clear();
    }

    std::vector<std::string> newLabels;
    arma::Row<double> newSubjectType;

    newLabels.insert(newLabels.begin(), std::string(labels[0].substr(0, labels[0].find_first_of('_'))));
    newSubjectType.resize(1);
    newSubjectType[0] = subjectType[0];

    newSubjectType.print();std::cout<<"\n";

    for(int i=1; i<labels.size(); i++){
        if(labels[i].substr(0, labels[i].find_first_of('_')).compare(labels[i - 1].substr(0, labels[i - 1].find_first_of('_'))) != 0){
          newLabels.push_back(std::string(labels[i].substr(0, labels[i].find_first_of('_')).data()));
          newSubjectType.resize(newSubjectType.size()+1);
          newSubjectType[newSubjectType.size()-1] = subjectType[i];
        }
    }

    labels = newLabels;
    subjectType = newSubjectType;


    return newData;
}


/// Drops the rows passed on toDrop
/// \param data data to drop from
/// \param labels labels to drop from
/// \param subjectType subjectType to drop from
/// \param toDrop list of elements to drop
/// \return matrix after dropping
arma::mat dropRows(arma::mat data, std::vector<std::string>& labels, arma::rowvec& subjectType, std::vector<int> toDrop){

    for(int i=0;i<toDrop.size();i++){
        data.shed_col(toDrop[i]);
        labels.erase(labels.begin() + toDrop[i]);
        std::cout<<subjectType.size();
        subjectType.shed_col(toDrop[i]);
    }

    return data;
}



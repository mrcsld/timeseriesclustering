//
// Created by marco on 18/04/19.
//

#include "cleaningMethods.h"

arma::mat oneRowRemoveAvg(arma::mat data){

    int record_length = 100;
    data.each_col([record_length](arma::colvec& col){
        vector<double> avg(col.size()/record_length, 0);
        int count = 0;

        col.for_each([&avg,&count,record_length](double val){
            if(!isnan(val))
                avg[(count++)/record_length] += val;
            else
                count++;
        });

        for(double& x:avg){
            x /= record_length;
        }

        count = 0;
        col.for_each([avg,&count,record_length](double& val){
            val -= avg[(count++)/record_length];
        });

    });

    return data;
}

/// Standardize the data, bringing it to stdev = 1
/// \param data dataset to normalize
/// \return standardized dataset
arma::mat oneRowRemoveStd(arma::mat data){
    int record_length = 100;

    data.each_col([record_length](arma::colvec& col){
        vector<double> stdev(col.size()/record_length, 0);
        int count = 0;

        col.for_each([&stdev,&count,record_length](double val){
            if(!isnan(val))
                stdev[count/record_length] += pow(val,2);
            count++;
        });

        for(double& x : stdev){
            x /= record_length;
            x = sqrt(x);
        }


        count = 0;
        col.for_each([stdev,&count,record_length](double& val){
            val /= stdev[count++/record_length];
        });

    });

    return data;
}


arma::mat oneRowStandardize(arma::mat data)
{
    int record_length = 100;
    data = newDataExtractor1(data);

    vector<double> means(data.n_rows, 0);
    vector<double> stddevs(data.n_rows, 0);

    int index = 0;
    data.each_row([&means, &index](arma::rowvec row){
        int count = 0;
        row.for_each([&count, &means, index](double val){
            if(!isnan(val)){
                count++;
                means[index] += val;
            }
        });
        means[index] /= count;
        index++;
    });

    index = 0;
    data.each_row([&means,&stddevs, &index](arma::rowvec row){
        int count = 0;
        row.for_each([&count, &means, &stddevs, index](double val){
            if(!isnan(val)){
                count++;
                stddevs[index] += pow((val - means[index]),2);
            }
        });
        stddevs[index] = sqrt(stddevs[index]/count);
        index++;
    });

    index = 0;
    data.each_row([means,stddevs, &index](arma::rowvec& row){
        row.for_each([means, stddevs, index](double& val){
            if(!isnan(val)){
                val = (val - means[index]) / stddevs[index];
            }
        });
        index++;
    });

    data = newDataExtractor2(data);

    return data;
}

/// Normalize - EACH - vector, from each subject, in the span [0,1] (locally, not globally)
/// \param data dataset to normalize
/// \return normalized dataset
arma::mat oneRowNormalize(arma::mat data){
    int record_length = 100;
    data.each_col([record_length](arma::colvec& col){
        vector<double> maxes(col.size()/record_length, numeric_limits<double>::min());
        vector<double> mins(col.size()/record_length, numeric_limits<double>::max());

        int count = 0;
        col.for_each([&maxes,&mins,&count,record_length](double val){
            if(!isnan(val)) {
                maxes[count / record_length] = max(maxes[count / record_length], val);
                mins[count / record_length] = min(mins[count / record_length], val);
            }
            count++;
        });

        count = 0;
        col.for_each([maxes,mins,&count,record_length](double& val){
            val -= mins[count/record_length];
            val /= (maxes[count/record_length] - mins[count/record_length]);
            count++;
        });

    });

    return data;
}

/// Normalize each vector in the span [0,1], globally
/// \param data dataset to normalize
/// \return normalized dataset
arma::mat oneRowNormalize2(arma::mat data){
    int record_length = 100;
    vector<double> maxes(data.n_rows/record_length, numeric_limits<double>::min());
    vector<double> mins(data.n_rows/record_length, numeric_limits<double>::max());

    data.each_col([record_length,&maxes,&mins](arma::colvec& col){
        int count = 0;
        col.for_each([&maxes,&mins,&count,record_length](double val){
            if(!isnan(val)) {
                maxes[count / record_length] = max(maxes[count / record_length], val);
                mins[count / record_length] = min(mins[count / record_length], val);
            }
            count++;
        });
    });

    data.each_col([record_length, maxes, mins](arma::colvec& col){
        int count = 0;
        col.for_each([maxes,mins,&count,record_length](double& val){
            val -= mins[count/record_length];
            val /= (maxes[count/record_length] - mins[count/record_length]);
            count++;
        });
    });

    return data;
}


/// Thought for 1 point per row
/// \param data
/// \return
arma::mat oneRowSubstituteNaN(arma::mat data){
    data.each_row([](arma::rowvec& row){
        double avg = 0;
        int count = 0;

        row.for_each([&avg, &count](double val){
            if(!isnan(val)){
                avg += val;
                count++;
            }
        });

        avg /= count;

        row.for_each([avg](double& val){
            if(isnan(val)){
                val = avg;
            }
        });
    });


    return data;
}

arma::mat removeAvg(arma::mat data){
    int record_length = 3;

    data.each_row([record_length](arma::rowvec& row){
        vector<double> avg(row.size()/record_length, 0);
        int count = 0;

        row.for_each([&avg,&count,record_length](double val){
            if(!isnan(val))
                avg[(count++)/record_length] += val;
            else
                count++;
        });

        cout<<"\n AVERAGES\n";

        for(double& x : avg){
            x /= record_length;
            cout<<x<<"\n";
        }

        count = 0;
        row.for_each([avg,&count,record_length](double& val){
            val -= avg[(count++)/record_length];
        });


    });

    return data;
}


arma::mat removeStd(arma::mat data){
    int record_length = 100;

    data.each_row([record_length](arma::rowvec& row){
        double stdev = 0;
        int count = 0;

        row.for_each([&stdev,&count](double val){
            if(!isnan(val))
                stdev += pow(val,2);
                count++;
        });

        cout<<"\n STD-DEV\n";

        stdev /= count;
        stdev = sqrt(stdev);
        cout<<stdev<<"\n";


        row.for_each([stdev](double& val){
            val /= stdev;
        });


    });

    return data;
}


/// Thought for 1 point per row
/// \param data
/// \return
arma::mat substituteNaN(arma::mat data){
    int record_length = 3;

    cout<<"NAN CHECK\n";

    arma::rowvec check = data.row(2);
    cout << check.t();
    std::vector<double> checkavg(record_length, 0);
    std::vector<int> checknum(record_length,0);
    int counter = 0;

    check.for_each([&checkavg, &checknum, &counter](double val){
        if (!isnan(val)){
            checkavg[counter%checkavg.size()] += val;
            checknum[counter%checkavg.size()]++;
        }
        counter++;
    });

    cout<<"TEST\n";
    for(int i=0; i<checkavg.size(); i++){
        checkavg[i] /= checknum[i];
        cout<<checkavg[i]<<"\t";
    }
    cout<<"\n\n";

    counter = 0;
    data.each_row([&checkavg, &counter](arma::rowvec& row){
        counter = 0;
        row.for_each([&checkavg, &counter](double& val){
            if (isnan(val)){
                val = checkavg[counter++%checkavg.size()];
            }
        });
    });


    return data;
}
